from selenium import webdriver
from axe_selenium_python import Axe
from webdriver_manager.firefox import GeckoDriverManager

def test_matt():
    driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    driver.get("https://mattbelanger321.github.io/")
    axe = Axe(driver)
    # Inject axe-core javascript into page.
    axe.inject()
    # Run axe accessibility checks.
    results = axe.run()
    # Write results to file
    axe.write_results(results, 'a11y.json')
    driver.close()
    # Assert no violations are found
    assert len(results["violations"]) == 0, axe.report(results["violations"])

if __name__ == '__main__':
	test_matt()